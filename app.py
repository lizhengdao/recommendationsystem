from flask import Flask, request, jsonify, render_template
import numpy as np
import pandas as pd
from csv import writer
import math
from collections import defaultdict
import csv
from apyori import apriori


app = Flask(__name__)


@app.route("/")
def home():
    return render_template("index.html")

@app.route("/mine")
def mine():
    finalDataFrame = aprioriFunction()
    return render_template("mine.html", column_names=finalDataFrame.columns.values, row_data=list(finalDataFrame.values.tolist()), zip=zip)


@app.route("/buy")
def show():
    return render_template("api.html")

@app.route("/buy", methods=["POST"])
def buy():
    products = request.form.getlist("product")
    with open("Market_Basket_Optimisation.csv", "a+", newline="") as write_obj:
        csv_writer = writer(write_obj)            
        csv_writer.writerow(products)
    finalDataFrame = aprioriFunction()
    rec=[]
    for i in finalDataFrame.index:
        check_buy = set(finalDataFrame['Bought'][i])
        buy = set(products)
        if (check_buy.issubset(buy)):
            for w in finalDataFrame['Recommendation'][i]:
                rec.append(w)
        if (buy.issubset(check_buy)):
            temp = list(check_buy.difference(buy))
            for w in temp:
                rec.append(w)
        if (len(rec)>5):
            break
    recList = list(set(rec))
    highestSellingProducts = getHighestSellingProducts()
    for w in highestSellingProducts:
        if (len(recList) < 5):
            recList.append(w)
        else:
            break
    return render_template("api.html", rec_text="You May Also Buy . . .", products=recList)


def aprioriFunction():
    dataset = pd.read_csv('Market_Basket_Optimisation.csv', header = None)
    transactions = []
    for i in range(0, len(dataset)):
        transactions.append([str(dataset.values[i,j]) for j in range(0, 20)])
    # Training the Apriori model on the dataset
    rules = apriori(transactions, min_support = 0.003, min_confidence = 0.2, min_lift = 3, min_length = 2)
    result = list(rules)
    dataFrameResult = pd.DataFrame(result)
    dataFrameResult = dataFrameResult.dropna()
    support = dataFrameResult.support
    bought = []
    recommendation = []
    confidence = []
    lift = []
    for i in range(dataFrameResult.shape[0]):
        singleList = dataFrameResult["ordered_statistics"][i][0]
        bought.append(list(singleList[0]))
        recommendation.append(list(singleList[1]))
        confidence.append(singleList[2])
        lift.append(singleList[3])
    data = {"Bought": bought, "Recommendation": recommendation, "Support": support, "Confidence": confidence, "Lift": lift}
    finalDataFrame = pd.DataFrame(data)
    finalDataFrame = finalDataFrame.sort_values(by ='Lift', ascending=False)
    return finalDataFrame

def getHighestSellingProducts():
    with open('Market_Basket_Optimisation.csv', 'r') as f:
        file = csv.reader(f)
        l = list(file)
    products = []
    for i in l:
        for j in i:
            products.append(j)
    productCount = defaultdict(int) # default value of int is 0
    for w in products:
        productCount[w]=productCount[w]+1
    sortedProducts = sorted(productCount.items(), key=lambda x: x[1], reverse=True)
    result = []
    for w in sortedProducts:
        result.append(w[0])
        if (len(result)>=5):
            break
    return result